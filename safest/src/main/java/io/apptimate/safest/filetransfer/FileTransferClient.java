import java.io.*;

import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

public class FileTransferClient {

    private FTPClient client;

    public FileTransferClient() {
        client = new FTPClient();
    }


    private boolean isConnected(){
        return client.isConnected();
    }

    private void connect() throws FileTransferConnectionFailedException{

        try {
            client.connect("jonashall.in");
            client.login("ftpuser", "ftppwd");
            client.setFileType(FTP.BINARY_FILE_TYPE);
        }catch (IOException e){
            throw new FileTransferConnectionFailedException();
        }
    }



    public void upload(String id, InputStream stream, final ProgressUpdater pu) throws FileTransferConnectionFailedException{

        if (!isConnected()) {
            connect();
        }


        CountingInputStream cis = new CountingInputStream(stream){
            @Override
            protected void afterRead(int n) {
                if (n == 0)
                    pu.byteChanged(getCount());

                super.afterRead(n);
            }
        };

        try {
            client.storeFile(id, cis);
            cis.close();
        }catch (IOException e) {
            throw new FileTransferConnectionFailedException();
        }
    }


    public void uploadFrom(String id, File f, final ProgressUpdater pu) throws FileTransferConnectionFailedException,FileTransferUnavailableLocalFileException{

        InputStream is = null;

        try {
            is = new FileInputStream(f);
        }catch (FileNotFoundException e){
            throw new FileTransferUnavailableLocalFileException();
        }

        upload(id, is, pu);

        try {
            is.close();
        }catch (IOException e){
            throw new FileTransferConnectionFailedException();
        }
    }

    public InputStream download(String id, final DownloadProgressUpdater dpu) throws FileTransferConnectionFailedException, FileTransferFileUnavailableException {

        if (!isConnected()){
            connect();
        }

        String remoteFile = id;

        try {

            client.sendCommand("SIZE", remoteFile);
            String reply = client.getReplyString();
            String [] resp = reply.split("\\s");

            if (resp.length != 2){
                throw new FileTransferFileUnavailableException(new Exception("Unrecognized server reply:"+reply));
            }
            int code = Integer.valueOf(resp[0]);

            if (!FTPReply.isPositiveCompletion(code)){
                throw new FileTransferFileUnavailableException(new Exception("Unrecognized server reply:"+reply));
            }

            dpu.setSize(Long.valueOf(resp[1]));

        } catch (IOException e) {
            throw new FileTransferConnectionFailedException(e);
        }

        ByteArrayOutputStream ds = new ByteArrayOutputStream();

        CountingOutputStream cos = new CountingOutputStream(ds){
            protected void beforeWrite(int n){
                super.beforeWrite(n);
                dpu.byteChanged(getCount());
            }
        };

        try {

            if (!client.retrieveFile(remoteFile, cos)){
                throw new FileTransferConnectionFailedException();
            }else{
                cos.flush();
                cos.close();
            }
        } catch (IOException e) {
            throw new FileTransferConnectionFailedException(e);
        }

        System.out.println("here");

        if (!FTPReply.isPositiveCompletion(client.getReplyCode())){

                throw new FileTransferFileUnavailableException();
        }

/*
        try {
            client.completePendingCommand();
        } catch (IOException e) {
            throw new FileTransferConnectionFailedException();
        }
*/
        InputStream is = new ByteArrayInputStream(ds.toByteArray());

        try {
            ds.close();
        }catch (IOException e){
            throw new FileTransferConnectionFailedException(e);
        }
        return is;
    }


    public void downloadTo(String id, File f, final DownloadProgressUpdater dpu) throws FileTransferConnectionFailedException, FileTransferFileUnavailableException, FileTransferUnavailableLocalFileException{

        InputStream is = download(id,dpu);

        FileOutputStream output = null;

        try {
            output = new FileOutputStream(f);
        }catch (FileNotFoundException e){
            throw new FileTransferUnavailableLocalFileException(e);
        }

        try
        {
            byte[] buffer = new byte[1024]; // Adjust if you want
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1)
            {
                output.write(buffer, 0, bytesRead);
            }

            output.flush();
            output.close();
            is.close();
        }catch (IOException e){
            throw new FileTransferFileUnavailableException();
        }
    }

    private void print(Object s){System.out.println(s+"");}


    public static void main(String[] trosor) throws Exception{

        FileTransferClient c = new FileTransferClient();

        Test t = new Test();


        File f = new File("big.txt");
        t.setSize(f.length());
        c.uploadFrom("big2.txt", f, t);
        System.out.println("upload complete");
        t.setSize(f.length());
        c.uploadFrom("big2.txt", f, t);
        System.out.println("upload complete");


        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        System.out.println("download complete");
        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        System.out.println("download complete");
        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        System.out.println("download complete");
        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        t.setSize(f.length());
        c.uploadFrom("big2.txt", f, t);
        System.out.println("upload complete");
        System.out.println("download complete");
        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        System.out.println("download complete");
        c.downloadTo("big2.txt",new File("./big2.txt"),t);
        System.out.println("download complete");
        t.setSize(f.length());
        c.uploadFrom("big2.txt", f, t);
        System.out.println("upload complete");



        System.out.println("donezo");

    }

    public interface ProgressUpdater{
        public void byteChanged(long total);
    }


    public interface DownloadProgressUpdater extends ProgressUpdater{

        public void setSize(long bytes);
    }

    static public class Test implements DownloadProgressUpdater{

        private long s;

        public void setSize(long bytes){
            s = bytes;
        }

        public void byteChanged(long tot){

            if (tot % (s/8) == 0) {
                System.out.println(String.format("%.1f%% done", (tot / (s / 100.0))));
            }
        }
    }

    public class FileTransferFileUnavailableException extends Exception{
        public FileTransferFileUnavailableException(){super();}
        public FileTransferFileUnavailableException(Exception e){super(e);}
    }
    public class FileTransferConnectionFailedException extends Exception{
        public FileTransferConnectionFailedException(){super();}
        public FileTransferConnectionFailedException(Exception e){super(e);}
    }
    public class FileTransferUnavailableLocalFileException extends Exception{
        public FileTransferUnavailableLocalFileException(){super();}
        public FileTransferUnavailableLocalFileException(Exception e){super(e);}
    }


}
